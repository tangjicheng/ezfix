#include <quickfix/Application.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/FixFieldNumbers.h>
#include <quickfix/FixFields.h>
#include <quickfix/FixValues.h>
#include <quickfix/Log.h>
#include <quickfix/Message.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/SocketAcceptor.h>
#include <quickfix/fix42/ExecutionReport.h>
#include <quickfix/fix42/NewOrderSingle.h>
#include <quickfix/fix44/ExecutionReport.h>
#include <quickfix/fix44/NewOrderSingle.h>
#include <cmath>
#include <cstdint>
#include <mutex>
#include <optional>
#include <string>
#include <unordered_map>
#include "quickfix/EzSettings.h"
#include "spdlog/spdlog.h"

using spdlog::error;
using spdlog::info;
using spdlog::warn;

std::string replace(const std::string& input) {
    std::string output = input;

    for (char& ch : output) {
        if (ch == '\x01') {
            ch = '|';
        }
    }
    return output;
}

FIX::SocketAcceptor* global_acceptor = nullptr;
FIX::Dictionary global_app_setting;

struct VenueSettings {
    std::string VenueID = "";
    double Ratio = 10;
};

class OrderIDManager {
   public:
    static OrderIDManager& instance() {
        static OrderIDManager instance;
        return instance;
    }

    OrderIDManager(const OrderIDManager&) = delete;
    OrderIDManager& operator=(const OrderIDManager&) = delete;

    uint64_t getOrderID(const std::string& key) {
        std::lock_guard<std::mutex> lock(mutex_);
        auto it = order_map_.find(key);
        if (it != order_map_.end()) {
            return it->second;
        } else {
            uint64_t new_id = ++order_id_counter_;
            order_map_[key] = new_id;
            return new_id;
        }
    }

   private:
    OrderIDManager() : order_id_counter_(0) {
    }

    std::unordered_map<std::string, uint64_t> order_map_;
    uint64_t order_id_counter_;
    std::mutex mutex_;
};

FIX::Message gen_ack(const FIX::Message& new_order,
                     [[maybe_unused]] const VenueSettings& venue_settings) {
    FIX::Message ack_msg(new_order);
    FIX::Header& header = ack_msg.getHeader();
    header.setField(FIX::FIELD::MsgType, FIX::MsgType_ExecutionReport);

    std::string cl_ord_id = ack_msg.getField(FIX::FIELD::ClOrdID);
    uint64_t order_id = OrderIDManager::instance().getOrderID(cl_ord_id);
    ack_msg.setField(FIX::FIELD::OrderID, std::to_string(order_id));

    ack_msg.setField(FIX::FIELD::AvgPx, std::string("0"));
    ack_msg.setField(FIX::FIELD::CumQty, std::string("0"));
    ack_msg.setField(FIX::FIELD::ExecID, venue_settings.VenueID + std::to_string(order_id));
    ack_msg.setField(FIX::FIELD::ExecTransType, std::string(1, FIX::ExecTransType_NEW));
    ack_msg.setField(FIX::FIELD::LastPx, std::string("0"));
    ack_msg.setField(FIX::FIELD::LastQty, std::string("0"));
    ack_msg.setField(FIX::FIELD::OrdStatus, std::string(1, FIX::OrdStatus_NEW));
    ack_msg.setField(FIX::FIELD::ExecType, std::string(1, FIX::ExecType_NEW));
    ack_msg.setField(FIX::FIELD::LeavesQty, new_order.getField(FIX::FIELD::OrderQty));

    return ack_msg;
}

FIX::Message gen_reject(const FIX::Message& new_order,
                        [[maybe_unused]] const VenueSettings& venue_settings) {
    FIX::Message reject(new_order);
    FIX::Header& header = reject.getHeader();
    header.setField(FIX::FIELD::MsgType, FIX::MsgType_ExecutionReport);

    std::string cl_ord_id = reject.getField(FIX::FIELD::ClOrdID);
    uint64_t order_id = OrderIDManager::instance().getOrderID(cl_ord_id);
    reject.setField(FIX::FIELD::OrderID, std::to_string(order_id));

    reject.setField(FIX::FIELD::AvgPx, std::string("0"));
    reject.setField(FIX::FIELD::CumQty, std::string("0"));
    reject.setField(FIX::FIELD::ExecID, venue_settings.VenueID + std::to_string(order_id));
    reject.setField(FIX::FIELD::ExecTransType, std::string(1, FIX::ExecTransType_NEW));
    reject.setField(FIX::FIELD::LastPx, std::string("0"));
    reject.setField(FIX::FIELD::LastQty, std::string("0"));
    reject.setField(FIX::FIELD::OrdStatus, std::string(1, FIX::OrdStatus_REJECTED));
    reject.setField(FIX::FIELD::Text,
                    std::string("Emulator Reject Reason, This Session is setted to reject mode."));

    reject.setField(FIX::FIELD::ExecType, std::string(1, FIX::ExecType_REJECTED));
    reject.setField(FIX::FIELD::OrdRejReason, std::string("0"));
    reject.setField(FIX::FIELD::LeavesQty, new_order.getField(FIX::FIELD::OrderQty));

    return reject;
}

FIX::Message gen_fullfill(const FIX::Message& new_order, const VenueSettings& venue_settings) {
    FIX::Message fullfill_msg(new_order);
    FIX::Header& header = fullfill_msg.getHeader();
    header.setField(FIX::FIELD::MsgType, FIX::MsgType_ExecutionReport);

    std::string cl_ord_id = fullfill_msg.getField(FIX::FIELD::ClOrdID);
    uint64_t order_id = OrderIDManager::instance().getOrderID(cl_ord_id);
    fullfill_msg.setField(FIX::FIELD::OrderID, std::to_string(order_id));

    fullfill_msg.setField(FIX::FIELD::AvgPx, new_order.getField(FIX::FIELD::Price));
    fullfill_msg.setField(FIX::FIELD::CumQty, new_order.getField(FIX::FIELD::OrderQty));
    fullfill_msg.setField(FIX::FIELD::ExecID, venue_settings.VenueID + std::to_string(order_id) +
                                                  std::string(":") + std::to_string(order_id + 1));
    fullfill_msg.setField(FIX::FIELD::ExecTransType, std::string(1, FIX::ExecTransType_NEW));
    fullfill_msg.setField(FIX::FIELD::LastPx, fullfill_msg.getField(FIX::FIELD::Price));
    fullfill_msg.setField(FIX::FIELD::LastQty, fullfill_msg.getField(FIX::FIELD::OrderQty));
    fullfill_msg.setField(FIX::FIELD::OrdStatus, std::string(1, FIX::OrdStatus_FILLED));
    fullfill_msg.setField(FIX::FIELD::ExecType, std::string(1, FIX::ExecType_FILL));

    fullfill_msg.setField(FIX::FIELD::TradeReportID, std::to_string(order_id + 1000'000));
    fullfill_msg.setField(FIX::FIELD::LeavesQty, std::to_string(0));

    return fullfill_msg;
}

struct PartialResult {
    std::optional<FIX::Message> partialfill_msg = std::nullopt;
    std::optional<FIX::Message> cancel_msg = std::nullopt;
};

PartialResult gen_partialfill_and_reject(const FIX::Message& new_order,
                                         const VenueSettings& venue_settings) {
    PartialResult result;
    double new_order_qty = new_order.getField<FIX::OrderQty>().getValue();
    int executed_qty = round(new_order_qty * venue_settings.Ratio / 100.0);
    int leaves_qty = round(new_order_qty - executed_qty);
    if (executed_qty < 100) {
        auto cancel_msg = gen_reject(new_order, venue_settings);
        cancel_msg.setField(
            FIX::FIELD::Text,
            std::string(
                "Emulator Reject Reason, This Session is setted to partialfill mode. Ratio = ") +
                std::to_string(venue_settings.Ratio));
        result.cancel_msg = cancel_msg;
        return result;
    }

    // for partial fill message
    FIX::Message partialfill_msg(new_order);
    FIX::Header& header = partialfill_msg.getHeader();
    header.setField(FIX::FIELD::MsgType, FIX::MsgType_ExecutionReport);

    std::string cl_ord_id = partialfill_msg.getField(FIX::FIELD::ClOrdID);
    uint64_t order_id = OrderIDManager::instance().getOrderID(cl_ord_id);
    partialfill_msg.setField(FIX::FIELD::OrderID, std::to_string(order_id));

    partialfill_msg.setField(FIX::FIELD::AvgPx, new_order.getField(FIX::FIELD::Price));
    partialfill_msg.setField(FIX::FIELD::CumQty, std::to_string(executed_qty));
    partialfill_msg.setField(FIX::FIELD::ExecID, venue_settings.VenueID + std::to_string(order_id) +
                                                     std::string(":") +
                                                     std::to_string(order_id + 1));
    partialfill_msg.setField(FIX::FIELD::ExecTransType, std::string(1, FIX::ExecTransType_NEW));
    partialfill_msg.setField(FIX::FIELD::LastPx, partialfill_msg.getField(FIX::FIELD::Price));
    partialfill_msg.setField(FIX::FIELD::LastQty, std::to_string(executed_qty));
    partialfill_msg.setField(FIX::FIELD::OrdStatus,
                             std::string(1, FIX::OrdStatus_PARTIALLY_FILLED));
    partialfill_msg.setField(FIX::FIELD::ExecType, std::string(1, FIX::ExecType_PARTIAL_FILL));

    partialfill_msg.setField(FIX::FIELD::TradeReportID, std::to_string(order_id + 1000'000));
    partialfill_msg.setField(FIX::FIELD::LeavesQty, std::to_string(leaves_qty));

    result.partialfill_msg = partialfill_msg;

    // for reject message
    {
        FIX::Message reject(new_order);
        FIX::Header& header = reject.getHeader();
        header.setField(FIX::FIELD::MsgType, FIX::MsgType_ExecutionReport);

        std::string cl_ord_id = reject.getField(FIX::FIELD::ClOrdID);
        reject.setField(FIX::FIELD::OrderID, std::to_string(order_id));

        reject.setField(FIX::FIELD::AvgPx, std::string("0"));
        reject.setField(FIX::FIELD::CumQty, std::to_string(executed_qty));
        reject.setField(FIX::FIELD::ExecID, venue_settings.VenueID + std::to_string(order_id));
        reject.setField(FIX::FIELD::ExecTransType, std::string(1, FIX::ExecTransType_NEW));
        reject.setField(FIX::FIELD::LastPx, std::string("0"));
        reject.setField(FIX::FIELD::LastQty, std::string("0"));
        reject.setField(FIX::FIELD::OrdStatus, std::string(1, FIX::OrdStatus_REJECTED));
        reject.setField(
            FIX::FIELD::Text,
            std::string("Emulator Reason, This Session is setted to partialfill mode. Ratio = ") +
                std::to_string(round(venue_settings.Ratio)));

        reject.setField(FIX::FIELD::ExecType, std::string(1, FIX::ExecType_CANCELED));
        reject.setField(FIX::FIELD::OrdRejReason, std::string("0"));
        reject.setField(FIX::FIELD::LeavesQty, std::to_string(leaves_qty));

        result.cancel_msg = reject;
    }

    return result;
}

FIX::Message gen_unknown(const FIX::Message& msg) {
    FIX::Message unknown(msg);
    unknown.setField(FIX::FIELD::Text,
                     std::string("ERROR, Emulator does not know this message type"));
    return unknown;
}

class EchoApp : public FIX::Application {
   protected:
    void onCreate(const FIX::SessionID& sessionID) override {
        info("onCreate: {}", sessionID.toString());
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        info("onLogon: {}", sessionID.toString());
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        info("onLogout: {}", sessionID.toString());
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        info("fromAdmin: {} , {}", sessionID.toString(), replace(message.toString()));
    }

    void toAdmin(FIX::Message& message, const FIX::SessionID& sessionID) override {
        try {
            auto sessionSettings = global_acceptor->getSessionSettings(sessionID);
            if (sessionSettings->has("SenderSubID")) {
                std::string senderSubID = sessionSettings->getString("SenderSubID");
                FIX::Header& header = message.getHeader();
                header.setField(FIX::FIELD::SenderSubID, senderSubID);
            }
            if (sessionSettings->has("TargetSubID")) {
                std::string targetSubID = sessionSettings->getString("TargetSubID");
                FIX::Header& header = message.getHeader();
                header.setField(FIX::FIELD::TargetSubID, targetSubID);
            }
        } catch (const FIX::FieldNotFound& e) {
            std::cerr << "Field not found: " << e.what() << std::endl;
        } catch (const FIX::ConfigError& e) {
            std::cerr << "Config error: " << e.what() << std::endl;
        } catch (const std::exception& e) {
            std::cerr << "Exception: " << e.what() << std::endl;
        }

        info("toAdmin: {} , {}", sessionID.toString(), replace(message.toString()));
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        info("fromApp: {} , {}", sessionID.toString(), replace(message.toString()));

        try {
            if (message.getHeader().getField(FIX::FIELD::MsgType) == std::string("D")) {
                auto setting = global_acceptor->getSessionSettings(sessionID);

                VenueSettings venue_settings;
                if (setting->has("VENUE_ID")) {
                    venue_settings.VenueID = setting->getString("VENUE_ID");
                }
                if (setting->has("RATIO")) {
                    venue_settings.Ratio = setting->getDouble("RATIO");
                }

                if (setting->has("EMU_MODE") &&
                    setting->getString("EMU_MODE") == std::string("REJECT")) {
                    // REJECT mode
                    info("REJECT Mode is ON");
                    auto reject = gen_reject(message, venue_settings);
                    FIX::Session::sendToTarget(reject, sessionID);
                } else if (setting->getString("EMU_MODE") == std::string("FILL")) {
                    // FILL mode
                    info("FILL Mode is ON");
                    auto ack = gen_ack(message, venue_settings);
                    auto fullfill = gen_fullfill(message, venue_settings);
                    FIX::Session::sendToTarget(ack, sessionID);
                    FIX::Session::sendToTarget(fullfill, sessionID);
                } else if (setting->getString("EMU_MODE") == std::string("PART")) {
                    // Partial Fill mode
                    info("Partial FILL Mode is ON");
                    auto ack = gen_ack(message, venue_settings);
                    auto partial_result = gen_partialfill_and_reject(message, venue_settings);
                    FIX::Session::sendToTarget(ack, sessionID);
                    if (partial_result.partialfill_msg != std::nullopt) {
                        FIX::Session::sendToTarget(*partial_result.partialfill_msg, sessionID);
                    }
                    if (partial_result.cancel_msg != std::nullopt) {
                        FIX::Session::sendToTarget(*partial_result.cancel_msg, sessionID);
                    }
                }

            } else {
                auto unknown = gen_unknown(message);
                FIX::Session::sendToTarget(unknown, sessionID);
            }
        }

        catch (const FIX::FieldNotFound& e) {
            std::cerr << "Field not found: " << e.what() << std::endl;
        } catch (const FIX::ConfigError& e) {
            std::cerr << "Config error: " << e.what() << std::endl;
        } catch (const std::exception& e) {
            std::cerr << "Exception: " << e.what() << std::endl;
        }
    }

    void toApp(FIX::Message& message, const FIX::SessionID& sessionID) override {
        auto sess = FIX::Session::lookupSession(sessionID);
        message.setField(FIX::FIELD::TransactTime,
                         FIX::OrigSendingTime(sess->getSupportedTimestampPrecision()).getString());
        info("toApp: {} , {}", sessionID.toString(), replace(message.toString()));
    }
};

int main(int argc, char** argv) {
    if (argc != 2) {
        error("Usage: {} <config file>", argv[0]);
        return 1;
    }

    std::string configFile = argv[1];
    FIX::EzSettings ez_settings(configFile);
    FIX::SessionSettings settings = ez_settings.getSessionSettings();
    global_app_setting = ez_settings.getAppSettings();
    EchoApp application;
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);
    global_acceptor = &acceptor;

    try {
        acceptor.start();
        info("EqSOR Emulator started...");

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }

    } catch (const FIX::ConfigError& e) {
        error("Configuration error: {}", e.what());
        return 1;
    } catch (const FIX::RuntimeError& e) {
        error("Runtime error: {}", e.what());
        return 1;
    }

    acceptor.stop();
    return 0;
}
