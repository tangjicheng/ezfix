
#include <quickfix/Application.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/Session.h>
#include <quickfix/SocketAcceptor.h>
#include <quickfix/fix44/ExecutionReport.h>
#include <quickfix/fix44/MessageCracker.h>
#include <quickfix/fix44/NewOrderSingle.h>
#include <quickfix/fix44/OrderCancelRequest.h>
#include <atomic>
#include <chrono>
#include <cstdio>
#include <thread>

#define LOG(level, message)                                                                      \
    do {                                                                                         \
        char time_buffer[64];                                                                    \
        struct timeval tv;                                                                       \
        gettimeofday(&tv, nullptr);                                                              \
        struct tm* tm_INFO = localtime(&tv.tv_sec);                                              \
        strftime(time_buffer, sizeof(time_buffer), "%Y-%m-%d %H:%M:%S", tm_INFO);                \
        printf("[%s.%03ld] [PID %d] [%s] %s\n", time_buffer, tv.tv_usec / 1000, getpid(), level, \
               std::string(message).c_str());                                                    \
    } while (0)

#define INFO(message) LOG("INFO", message)

#define ERROR(message) LOG("ERROR", message)

std::atomic<u_int64_t> total_count = 0;

class BenchApp : public FIX::Application, public FIX44::MessageCracker {
   public:
    void onCreate(const FIX::SessionID& sessionID) override {
        INFO("onCreate: " + sessionID.toString());
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        INFO("onLogon: " + sessionID.toString());
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        INFO("onLogout: " + sessionID.toString());
    }

    void toAdmin(FIX::Message&, const FIX::SessionID&) override {
    }

    void toApp(FIX::Message&, const FIX::SessionID&) noexcept override {
    }

    void fromAdmin(const FIX::Message&, const FIX::SessionID&) noexcept override {
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) noexcept override {
        crack(message, sessionID);
    }

    void onMessage(const FIX44::NewOrderSingle&, const FIX::SessionID&) override {
        total_count += 1;
    }

    void onMessage(const FIX44::OrderCancelRequest&, const FIX::SessionID&) override {
        total_count += 1;
    }
};

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Usage: %s <config file>\n", argv[0]);
        return 1;
    }

    std::string configFile = argv[1];
    FIX::SessionSettings settings(configFile);
    BenchApp application;
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);

    try {
        acceptor.start();
        INFO("FIX Benchserver started...");

        auto previous_time = std::chrono::steady_clock::now();
        u_int64_t previous_count = 0;
        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
            auto current_time = std::chrono::steady_clock::now();
            u_int64_t current_count = total_count.load();

            auto count_diff = current_count - previous_count;
            auto time_diff =
                std::chrono::duration_cast<std::chrono::seconds>(current_time - previous_time)
                    .count();

            double message_rate = static_cast<double>(count_diff) / static_cast<double>(time_diff);
            printf("Speed: %.2f messages/second, total message count: %ld\n", message_rate,
                   current_count);

            previous_count = current_count;
            previous_time = current_time;
        }
    } catch (const FIX::ConfigError& e) {
        ERROR(std::string("Configuration error: ") + e.what());
        return 1;
    } catch (const FIX::RuntimeError& e) {
        ERROR(std::string("Runtime error: ") + e.what());
        return 1;
    }

    acceptor.stop();
    return 0;
}
