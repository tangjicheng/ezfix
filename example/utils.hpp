#pragma once
#include <string>
#include <string_view>
#include <regex>
#include <optional>
#include <fstream>

#include <quickfix/Message.h>


enum class Direction { ToPipe, ToSOH };

template <Direction d = Direction::ToPipe>
std::string replace(std::string_view input) {
    std::string output(input);

    if constexpr (d == Direction::ToPipe) {
        for (char& ch : output) {
            if (ch == '\x01') {
                ch = '|';
            }
        }
        return output;
    }

    if constexpr (d == Direction::ToSOH) {
        for (char& ch : output) {
            if (ch == '|') {
                ch = '\x01';
            }
        }
        return output;
    }

    return output;
}

inline std::string extractFIXPart(const std::string& input) {
    static const std::regex pattern(R"(8=FIX.*?\x0110=.*\x01)");

    std::smatch match;
    if (std::regex_search(input, match, pattern)) {
        return match.str();
    } else {
        return "";
    }
}

inline std::optional<FIX::Message> parseOneLine(const std::string& line) {
    std::string line_x01 = replace<Direction::ToSOH>(line);
    std::string raw_fix_message = extractFIXPart(line_x01);

    FIX::Message message;
    try {
        message.setString(raw_fix_message, false);
    } catch (const FIX::InvalidMessage& ex) {
        return std::nullopt;
    }

    return message;
}


inline std::vector<FIX::Message> parseOrdersFromFile(const std::string& file_path) {
    std::vector<FIX::Message> orders_vec;
    std::ifstream file(file_path);
    std::string line;

    if (!file) {
        std::cerr << "Unable to open file: " << file_path << std::endl;
        return orders_vec;
    }

    while (getline(file, line)) {
        if (line.find("35=D") != std::string::npos) {
            auto new_order_opt = parseOneLine(line);
            if (new_order_opt) {
                orders_vec.push_back(*new_order_opt);
            }
        } else if (line.find("35=F") != std::string::npos) {
            auto cancel_order_opt = parseOneLine(line);
            if (cancel_order_opt) {
                orders_vec.push_back(*cancel_order_opt);
            }
        }
    }

    file.close();
    return orders_vec;
}
