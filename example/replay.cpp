#include <quickfix/Application.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/Initiator.h>
#include <quickfix/Message.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/SocketInitiator.h>
#include <chrono>
#include <string>
#include <thread>

#include "utils.hpp"

class ReplayApp : public FIX::Application {
   public:
    ReplayApp(const std::vector<FIX::Message>& msg_vec) : msg_vec_(msg_vec) {
    }

    void onCreate(const FIX::SessionID& sessionID) override {
        printf("onCreate: %s\n", sessionID.toString().c_str());
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        printf("onLogon: %s\n", sessionID.toString().c_str());
        std::this_thread::sleep_for(std::chrono::seconds(1));

        for (auto& msg : msg_vec_) {
            FIX::Session::sendToTarget(msg, sessionID);
        }
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        printf("onLogout: %s\n", sessionID.toString().c_str());
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("fromAdmin: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

    void toAdmin(FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("toAdmin: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("fromApp: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

    void toApp(FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("toApp: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

   private:
    std::vector<FIX::Message> msg_vec_;
};

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Usage: %s <fix config file> <replay log file>\n", argv[0]);
        return 1;
    }

    std::string configFile = argv[1];
    FIX::SessionSettings settings(configFile);
    auto msg_vec = parseOrdersFromFile(argv[2]);

    ReplayApp application(msg_vec);
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketInitiator initiator(application, storeFactory, settings, logFactory);

    try {
        initiator.start();
        printf("Replay started...");

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }
    } catch (const FIX::ConfigError& e) {
        printf("Configuration error: %s\n", e.what());
        return 1;
    } catch (const FIX::RuntimeError& e) {
        printf("Runtime error: %s\n", e.what());
        return 1;
    } catch (const std::exception& e) {
        printf("Exception: %s\n", e.what());
    }

    initiator.stop();

    return 0;
}
