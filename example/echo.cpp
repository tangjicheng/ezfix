#include <quickfix/Application.h>
#include <quickfix/EzSettings.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/Message.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/SocketAcceptor.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <nlohmann/json.hpp>
#include "utils.hpp"

using json = nlohmann::json;

class FixResponse {
   public:
    FixResponse() {
        ResetPosition();
    }

    size_t Size() {
        return fix_responses_.size();
    }

    std::vector<FIX::Message> Next() {
        if (current_position_ == fix_responses_.end()) {
            return {};
        }
        auto result = current_position_->second;
        current_position_++;
        return result;
    }

    void ParseJson(const std::string& json_filename) {
        fix_responses_.clear();
        std::ifstream file(json_filename);
        if (!file.is_open()) {
            std::cerr << "ParseJson: Failed to open json file." << std::endl;
            return;
        }

        json json_data;
        try {
            file >> json_data;
        } catch (json::parse_error& e) {
            std::cerr << "ParseJson: JSON parse error: " << e.what() << std::endl;
            return;
        }

        file.close();

        auto response_json = json_data["response"];

        for (auto& [key, value] : response_json.items()) {
            int key_int = std::stoi(key);
            if (key_int > 0) {
                std::vector<FIX::Message> msg_vec;
                for (auto&& msg_json : value) {
                    FIX::Message msg;
                    std::string msg_str = msg_json.get<std::string>();
                    msg.setString(replace<Direction::ToSOH>(msg_str), false);
                    msg_vec.push_back(msg);
                }
                this->fix_responses_[key_int] = msg_vec;
            }
        }

        ResetPosition();
        return;
    }

   private:
    void ResetPosition() {
        this->current_position_ = fix_responses_.begin();
    }
    std::map<int, std::vector<FIX::Message>, std::less<int>> fix_responses_;
    std::map<int, std::vector<FIX::Message>>::iterator current_position_;
};

FixResponse echo_resp;

class EchoApp : public FIX::Application {
   protected:
    void onCreate(const FIX::SessionID& sessionID) override {
        printf("onCreate: %s\n", sessionID.toString().c_str());
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        printf("onLogon: %s\n", sessionID.toString().c_str());
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        printf("onLogout: %s\n", sessionID.toString().c_str());
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("fromAdmin: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

    void toAdmin(FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("toAdmin: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("fromApp: %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
        auto resp_vec = echo_resp.Next();
        for (auto&& msg : resp_vec) {
            FIX::Session::sendToTarget(msg, sessionID);
        }
    }

    void toApp(FIX::Message& message, const FIX::SessionID& sessionID) override {
        printf("toApp:  %s , %s\n", sessionID.toString().c_str(),
               replace(message.toString()).c_str());
    }
};

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Usage: %s <config file> <response json>\n", argv[0]);
        return 1;
    }

    if (argc == 3) {
        echo_resp.ParseJson(argv[2]);
    }

    std::string configFile = argv[1];
    FIX::EzSettings ez_settings(configFile);
    FIX::SessionSettings settings = ez_settings.getSessionSettings();
    EchoApp application;
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);

    auto app_setting = ez_settings.getAppSettings();
    std::cout << app_setting.getInt("hello") << std::endl;
    std::cout << app_setting.getString("world") << std::endl;

    try {
        acceptor.start();
        printf("FIX Server Echo started...\n");

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }
    } catch (const FIX::ConfigError& e) {
        printf("Configuration error: %s\n", e.what());
        return 1;
    } catch (const FIX::RuntimeError& e) {
        printf("Runtime error: %s\n", e.what());
        return 1;
    }

    acceptor.stop();
    return 0;
}
