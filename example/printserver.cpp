#include <quickfix/Application.h>
#include <quickfix/EzSettings.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/Message.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/SocketAcceptor.h>
#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include "utils.hpp"

#define LOG(fmt, ...)                                         \
    do {                                                      \
        std::stringstream ss;                                 \
        ss << std::this_thread::get_id();                     \
        printf("[tid %s] " fmt, ss.str().c_str(), ##__VA_ARGS__); \
    } while (0)

class PrintApp : public FIX::Application {
   protected:
    void onCreate(const FIX::SessionID& sessionID) override {
        LOG("onCreate: %s\n", sessionID.toString().c_str());
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        LOG("onLogon: %s\n", sessionID.toString().c_str());
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        LOG("onLogout: %s\n", sessionID.toString().c_str());
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        LOG("fromAdmin: %s , %s\n", sessionID.toString().c_str(),
            replace(message.toString()).c_str());
    }

    void toAdmin(FIX::Message& message, const FIX::SessionID& sessionID) override {
        LOG("toAdmin: %s , %s\n", sessionID.toString().c_str(),
            replace(message.toString()).c_str());
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) override {
        LOG("fromApp: %s , %s\n", sessionID.toString().c_str(),
            replace(message.toString()).c_str());
    }

    void toApp(FIX::Message& message, const FIX::SessionID& sessionID) override {
        LOG("toApp:  %s , %s\n", sessionID.toString().c_str(), replace(message.toString()).c_str());
    }
};

int main(int argc, char** argv) {
    if (argc < 2) {
        LOG("Usage: %s <config file>\n", argv[0]);
        return 1;
    }

    std::string configFile = argv[1];
    FIX::EzSettings ez_settings(configFile);
    FIX::SessionSettings settings = ez_settings.getSessionSettings();
    PrintApp application;
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);

    auto app_setting = ez_settings.getAppSettings();

    try {
        acceptor.start();
        LOG("FIX Print Server started...\n");

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }
    } catch (const FIX::ConfigError& e) {
        LOG("Configuration error: %s\n", e.what());
        return 1;
    } catch (const FIX::RuntimeError& e) {
        LOG("Runtime error: %s\n", e.what());
        return 1;
    }

    acceptor.stop();
    return 0;
}
