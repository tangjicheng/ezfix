#include <quickfix/Application.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/SocketAcceptor.h>
#include <quickfix/fix44/MessageCracker.h>
#include <quickfix/fix44/NewOrderCross.h>
#include <signal.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <thread>

#define LOG(level, message)                                                                      \
    do {                                                                                         \
        char time_buffer[64];                                                                    \
        struct timeval tv;                                                                       \
        gettimeofday(&tv, nullptr);                                                              \
        struct tm* tm_INFO = localtime(&tv.tv_sec);                                              \
        strftime(time_buffer, sizeof(time_buffer), "%Y-%m-%d %H:%M:%S", tm_INFO);                \
        printf("[%s.%03ld] [PID %d] [%s] %s\n", time_buffer, tv.tv_usec / 1000, getpid(), level, \
               (message).c_str());                                                               \
    } while (0)

#define INFO(message) LOG("INFO", message)

#define ERROR(message) LOG("ERROR", message)

std::string trim(const std::string& str) {
    size_t start = str.find_first_not_of(" \t\n\r\f\v");
    if (start == std::string::npos) {
        return "";
    }
    size_t end = str.find_last_not_of(" \t\n\r\f\v");
    return str.substr(start, end - start + 1);
}

std::string replaceSOH(const std::string& input) {
    std::string result(input);
    for (auto& ch : result) {
        if (ch == '\x01') {
            ch = '|';
        }
    }
    return result;
}

std::string replacePipe(const std::string& input) {
    std::string result(input);
    for (auto& ch : result) {
        if (ch == '|') {
            ch = '\x01';
        }
    }
    return result;
}

class EchoApp : public FIX::Application, public FIX44::MessageCracker {
   public:
    EchoApp(const FIX::SessionSettings& settings) : settings_(settings){};

    void onCreate(const FIX::SessionID& sessionID) override {
        INFO("onCreate: " + sessionID.toString() + "with port " +
             settings_.get(sessionID).getString(FIX::SOCKET_ACCEPT_PORT));
    }

    void onLogon(const FIX::SessionID& sessionID) override {
        INFO("onLogon: " + sessionID.toString());
    }

    void onLogout(const FIX::SessionID& sessionID) override {
        INFO("onLogout: " + sessionID.toString());
    }

    void toAdmin(FIX::Message&, const FIX::SessionID&) override {
    }

    void toApp(FIX::Message&, const FIX::SessionID&) noexcept override {
    }

    void fromAdmin(const FIX::Message& message, const FIX::SessionID&) noexcept override {
        INFO("fromAdmin: " + replaceSOH(message.toString()));
    }

    void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID) noexcept override {
        INFO("fromApp: " + replaceSOH(message.toString()));
        crack(message, sessionID);
    }

    void onMessage(const FIX44::NewOrderCross& new_order_cross, const FIX::SessionID&) override {
        INFO("onMessage NewOrderCross: " + replaceSOH(new_order_cross.toString()));
    }

   private:
    FIX::SessionSettings settings_;
};

void usage(const char* program_name) {
    std::cout << "Usage: " << program_name << " N arg1 arg2 argN" << std::endl;
    std::cout << "N: Number of worker processes to start (must be a positive integer)" << std::endl;
    std::cout << "arg1, arg2, argN: Arguments for each worker process" << std::endl;
}

void startWorker(int worker_num, const char* worker_arg) {
    prctl(PR_SET_PDEATHSIG, SIGTERM);

    INFO("Start Worker " + std::to_string(getpid()) + " with config " + worker_arg);

    if (getppid() == 1) {
        std::cout << "Worker " << worker_num << ": Parent has already died, exiting." << std::endl;
        exit(1);
    }

    std::string configFile(worker_arg);
    FIX::SessionSettings settings(configFile);
    EchoApp application(settings);
    FIX::FileStoreFactory storeFactory(settings);
    FIX::ScreenLogFactory logFactory(settings);
    FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);

    try {
        acceptor.start();

        while (true) {
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }

    } catch (const FIX::ConfigError& e) {
        ERROR("Configuration error: " + std::string(e.what()));
        return;
    } catch (const FIX::RuntimeError& e) {
        ERROR("Runtime error: {}" + std::string(e.what()));
        return;
    }

    acceptor.stop();

    std::cout << "Worker " << worker_num << " started with argument: " << worker_arg << std::endl;

    while (true) {
        std::cout << "Worker " << worker_num << " is processing..." << std::endl;
        sleep(3);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        usage(argv[0]);
        return 1;
    }

    char* endptr;
    long n = std::strtol(argv[1], &endptr, 10);
    if (*endptr != '\0' || n <= 0) {
        std::cout << "Error: N must be a positive integer." << std::endl;
        usage(argv[0]);
        return 1;
    }

    if (argc != n + 2) {
        std::cout << "Error: Insufficient arguments for worker processes." << std::endl;
        usage(argv[0]);
        return 1;
    }

    for (int i = 1; i <= n; ++i) {
        pid_t pid = fork();
        if (pid == -1) {
            std::cout << "Error: Failed to fork worker " << i << std::endl;
            return 1;
        } else if (pid == 0) {
            startWorker(i, argv[i + 1]);
            return 0;
        }
    }

    for (int i = 1; i <= n; ++i) {
        wait(NULL);
    }

    std::cout << "Master process exiting." << std::endl;
    return 0;
}
