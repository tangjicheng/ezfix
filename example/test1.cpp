#include <iostream>
#include "quickfix/config.h"
int main() {
#ifdef HAVE_CXX17
    std::cout << "HAVE CXX17" << std::endl;
#else
    std::cout << "NO CXX17" << std::endl;
#endif
    return 0;
}