#include <gtest/gtest.h>

#include <string>

TEST(HelloWorldTest, BasicAssertions) {
    std::string s("Hello, World!");
    EXPECT_EQ(s, "Hello, World!");
}

