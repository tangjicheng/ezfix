#pragma once
#include "SessionSettings.h"
#include <string>

namespace FIX {
class EzSettings {
   public:
    EzSettings(const std::string& filename);

    SessionSettings getSessionSettings() {
        return sess_;
    }

    Dictionary getAppSettings() {
        return app_;
    }

   private:
    SessionSettings sess_;
    Dictionary app_;
};
}  // namespace FIX
