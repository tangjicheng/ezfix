#include "EzSettings.h"
#include "Settings.h"
#include <fstream>

namespace FIX {
    EzSettings::EzSettings(const std::string& filename) {
        // parse original setting
        std::ifstream fstream( filename.c_str() );
        if ( !fstream.is_open() )
            throw ConfigError( ( "File " + filename + " not found" ).c_str() );
        fstream >> this->sess_;
        fstream.close();

        // parse application setting
        fstream.open(filename.c_str());
        Settings all_setting;
        fstream >> all_setting;

        Settings::Sections section = all_setting.get( "APP" );
        if ( section.size() )
            this->app_ = section[ 0 ];
    }
}