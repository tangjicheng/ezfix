# ezfix

```bash
podman build -t tang2432/ezfix:latest .
podman push tang2432/ezfix:latest
```

## description
original source code: https://github.com/quickfix/quickfix  

## How to use
```
include_directories(${THIRD_PARTY_DIR}/ezfix)
add_subdirectory(${THIRD_PARTY_DIR}/ezfix)
```
