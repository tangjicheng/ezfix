cmake_minimum_required(VERSION 3.10)

project(quickfix VERSION 2.0 LANGUAGES CXX C)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_BUILD_TYPE RelWithDebInfo)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -fopenmp ")

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(quickfix)

add_subdirectory(example)

option(ENABLE_GTEST "Enable Google Test" OFF)

if (ENABLE_GTEST)
    add_subdirectory(test)
endif()